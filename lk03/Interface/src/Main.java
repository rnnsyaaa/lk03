/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas pertambahan dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa interface akan lebih cocok digunakan daripada abstract class?
 * "Pada konteks kalkulator interface akan lebih cocok digunakan daripada abstract class apabila saat di run
   method dipisah dari implement class,karena pada interface tidak di jalankan implementasi default melainkan
   implementasi class dari setiap class secara spesifik.Selain itu apabila nanti akan ada tambahan class untuk
   menambah fitur bentuk operasi hitung seperti, pembagian,akar,konversi,dsb. Interface tidak akan mengubah
   method yang sudah ada sebelumnya dan akan menjalankan implementasi interface sesuai yang dibutuhkan.
   Alasan-alasan tersebut yang membuat interface dianggap lebih cocok atau lebih fleksible untuk digunakan. "
 */

import java.util.*;

public class Main {   
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operan1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operan2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        double result = kalkulator.hitung(operan1, operan2);
        System.out.println("Result: " + result);
    }
}